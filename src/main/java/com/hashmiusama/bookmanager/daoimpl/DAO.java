package com.hashmiusama.bookmanager.daoimpl;

import java.util.ArrayList;

/**
 * Created by uhashmi on 5/26/2016.
 */
public interface DAO {
    ArrayList<Object> getAll();
    ArrayList<Object> getByName(String name);
    Object getById(int id);
    boolean insert(Object object);
    boolean update(Object object);
    boolean delete(Object object);
}
