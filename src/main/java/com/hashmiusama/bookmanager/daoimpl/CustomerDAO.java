package com.hashmiusama.bookmanager.daoimpl;

import com.hashmiusama.bookmanager.facade.HibernateSessionManager;
import com.hashmiusama.bookmanager.models.CustomerModel;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import java.util.ArrayList;

/**
 * Created by uhashmi on 5/26/2016.
 */
public class CustomerDAO implements DAO {
    Session session;
    public CustomerDAO(){
        try {
            HibernateSessionManager.buildSessionFactory("customer");
            session = HibernateSessionManager.getSessionFactory().openSession();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public ArrayList<Object> getAll() {
        return (ArrayList<Object>) session.createCriteria(CustomerModel.class).list();
    }

    public ArrayList<Object> getByName(String name) {
        Criteria criteria = session.createCriteria(CustomerModel.class).add(Restrictions.eq("name",name));
        return (ArrayList<Object>) criteria.list();
    }

    public ArrayList<Object> getByPhoneNumber(String name) {
        Criteria criteria = session.createCriteria(CustomerModel.class).add(Restrictions.eq("phoneNumber",name));
        return (ArrayList<Object>) criteria.list();
    }

    public Object getById(int id) {
        System.out.println("Getting book by id "+ id);
        CustomerModel customer = null;
        customer = (CustomerModel)session.get(CustomerModel.class, id);
        return customer;
    }

    public boolean insert(Object object) {
        CustomerModel customer = (CustomerModel) object;
        System.out.println(customer.toString());
        boolean returnValue = false;
        if(Integer.parseInt(session.save(customer).toString()) > 0) {
            returnValue = true;
            session.beginTransaction().commit();
        }
        return returnValue;
    }

    public boolean update(Object object) {
        CustomerModel customer = (CustomerModel) object;
        try {
            session.update(customer);
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
        session.beginTransaction().commit();
        return true;
    }

    public boolean delete(Object object) {
        CustomerModel customer = (CustomerModel) object;
        try {
            session.delete(customer);
            session.beginTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public void closeSession(){
        session.close();
    }
}
