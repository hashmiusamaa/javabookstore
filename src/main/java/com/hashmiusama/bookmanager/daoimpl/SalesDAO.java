package com.hashmiusama.bookmanager.daoimpl;

import com.hashmiusama.bookmanager.facade.HibernateSessionManager;
import com.hashmiusama.bookmanager.models.EmployeeModel;
import com.hashmiusama.bookmanager.models.SalesModel;
import org.hibernate.Criteria;
import org.hibernate.Session;

import java.util.ArrayList;

/**
 * Created by uhashmi on 5/31/2016.
 */
public class SalesDAO{
    Session session;
    public SalesDAO(){
        try {
            HibernateSessionManager.buildSessionFactory("sales");
            session = HibernateSessionManager.getSessionFactory().openSession();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public ArrayList<Object> getAll() {
        Criteria criteria = session.createCriteria(SalesModel.class);
        return (ArrayList<Object>) criteria.list();
    }

    public Object getById(int id) {
        SalesModel sale = new SalesModel();
        sale = (SalesModel) session.get(SalesModel.class, id);
        return sale;
    }

    public boolean insert(Object object) {
        SalesModel sale = (SalesModel) object;
        CustomerDAO customerDAO = new CustomerDAO();
        BookDAO bookDAO = new BookDAO();
        if(customerDAO.getById(sale.getCustomerId()) != null && bookDAO.getById(sale.getBookId()) != null) {
            System.out.println(sale.toString());
            boolean returnValue = false;
            if (Integer.parseInt(session.save(sale).toString()) > 0) {
                returnValue = true;
                session.beginTransaction().commit();
            }
            return returnValue;
        }
        return false;
    }

    public boolean delete(Object object) {
        SalesModel sale = (SalesModel) object;
        try {
            session.delete(sale);
            session.beginTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
