package com.hashmiusama.bookmanager.daoimpl;

import com.hashmiusama.bookmanager.facade.HibernateSessionManager;
import com.hashmiusama.bookmanager.models.BookModel;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import sun.rmi.runtime.Log;

import java.util.ArrayList;

/**
 * Created by uhashmi on 5/26/2016.
 */
public class BookDAO implements DAO {
    final static Logger logger = Logger.getLogger(BookDAO.class);
    Session session;
    public BookDAO(){
        try {
            HibernateSessionManager.buildSessionFactory("book");
            session = HibernateSessionManager.getSessionFactory().openSession();
            logger.log(Level.ERROR,"session generated.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public ArrayList<Object> getAll() {
        return (ArrayList<Object>) session.createCriteria(BookModel.class).list();
    }

    public ArrayList<Object> getByName(String name) {
        System.out.println(name+" is chosen.");
        Criteria criteria = session.createCriteria(BookModel.class).add(Restrictions.eq("name",name));
        return (ArrayList<Object>) criteria.list();
    }

    public ArrayList<Object> getByAuthor(String name) {
        Criteria criteria = session.createCriteria(BookModel.class).add(Restrictions.eq("author",name));
        return (ArrayList<Object>) criteria.list();
    }

    public Object getById(int id) {
        System.out.println("Getting book by id "+ id);
        BookModel book = null;
        book = (BookModel)session.get(BookModel.class, id);
        return book;
    }

    public boolean insert(Object object) {
        BookModel book = (BookModel) object;
        boolean returnValue = false;
        if(Integer.parseInt(session.save(book).toString()) > 0) {
            returnValue = true;
            session.beginTransaction().commit();
        }
        return returnValue;
    }

    public boolean update(Object object) {
        BookModel book = (BookModel) object;
        try {
            session.saveOrUpdate(book);
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
        session.beginTransaction().commit();
        return true;
    }

    public boolean delete(Object object) {
        BookModel book = (BookModel) object;
        try {
            session.delete(book);
            session.beginTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public void closeSession(){
        session.close();
    }
}
