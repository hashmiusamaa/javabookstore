package com.hashmiusama.bookmanager.controller;

import com.hashmiusama.bookmanager.daoimpl.SalesDAO;
import com.hashmiusama.bookmanager.models.SalesModel;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;

/**
 * Created by uhashmi on 5/31/2016.
 */
@WebServlet(name = "SalesController",
        urlPatterns = "/sell")
public class SalesController extends HttpServlet{
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out = resp.getWriter();
        SalesModel model = new SalesModel();
        System.out.println(req.getParameter("customerid") + " ------ " + req.getParameter("bookid"));
        model.setSupervisorId(Integer.parseInt(req.getHeader("sid")));
        model.setCustomerId(Integer.parseInt(req.getParameter("customerid")));
        model.setBookId(Integer.parseInt(req.getParameter("bookid")));

        if(model.getBookId() == 0 && model.getCustomerId() == 0){
            out.print(false);
        }else{
            SalesDAO dao = new SalesDAO();
            out.print(dao.insert(model));
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ArrayList<Object> sales = null;
        SalesDAO dao = new SalesDAO();
        if(req.getParameter("phoneNumber")== null && req.getParameter("id")== null && req.getParameter("name")== null){
            sales = dao.getAll();
            System.out.println(sales.toString());
            resp.getWriter().print(sales.toString());
        }
    }
}
