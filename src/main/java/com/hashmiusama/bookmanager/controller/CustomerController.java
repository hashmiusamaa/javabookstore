package com.hashmiusama.bookmanager.controller;

import com.hashmiusama.bookmanager.daoimpl.CustomerDAO;
import com.hashmiusama.bookmanager.models.CustomerModel;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by uhashmi on 5/26/2016.
 */
@WebServlet(name = "CustomerController",
        urlPatterns = "/customer")
public class CustomerController extends HttpServlet {
    CustomerDAO dao = new CustomerDAO();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = "nulls";
        String name = "nulls";
        String phoneNumber = "nulls";
        CustomerModel customer = new CustomerModel();
        ArrayList<Object> customers = null;
        PrintWriter out = resp.getWriter();
        try{
            name = req.getParameter("name");
            if(!name.equals("nulls")){
                try{
                    customers = dao.getByName(name);
                    System.out.println(customers.toString());
                    out.print(customers.toString());
                }catch(NullPointerException e){
                    System.out.println("[]");
                    e.printStackTrace();
                }
            }
        }
        catch(NullPointerException e){}
        try{
            String idea = req.getParameter("id");
            id = idea;
            if(!id.equals("nulls")){
                try {
                    customer = (CustomerModel) dao.getById(Integer.parseInt(id));
                    out.print(customer.toString());
                }catch(NullPointerException e){
                    e.printStackTrace();
                    out.println("{}");
                }
            }
        }
        catch(NullPointerException e){}
        try{
            phoneNumber = req.getParameter("phoneNumber");
            if(!phoneNumber.equals("nulls")){
                try{
                    customers = dao.getByPhoneNumber(phoneNumber);
                    System.out.println(customers.toString());
                    out.print(customers.toString());
                }catch(NullPointerException e){
                    System.out.println("[]");
                    e.printStackTrace();
                }
            }
        }
        catch(NullPointerException e){}
        if(req.getParameter("phoneNumber")== null && req.getParameter("id")== null && req.getParameter("name")== null){
            customers = dao.getAll();
            System.out.println(customers.toString());
            out.print(customers.toString());
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try{
            int id = Integer.parseInt(req.getParameter("id"));
            CustomerModel customer = (CustomerModel) dao.getById(id);
            resp.getWriter().print(dao.delete(customer));
        }catch(NullPointerException e){}
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        CustomerModel customer = new CustomerModel();
        PrintWriter out = resp.getWriter();
        try{
            customer.setName(req.getParameter("name"));
            customer.setAddress(req.getParameter("address"));
            customer.setPhoneNumber(new BigInteger(req.getParameter("phoneNumber")));
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date myDate = null;
            java.sql.Date sqlDate = null;
            try {
                myDate = (Date) formatter.parse(req.getParameter("joiningDate"));
                sqlDate = new java.sql.Date(myDate.getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            customer.setJoiningDate(sqlDate);
            System.out.println("inserting "+customer.toString());
            if(dao.insert(customer)){
                out.print(true);
            }else{
                out.print(false);
            }
        }catch(NullPointerException e){e.printStackTrace();}
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        CustomerModel customer = new CustomerModel();
        PrintWriter out = resp.getWriter();
        try{
            System.out.println(Integer.parseInt(req.getParameter("id")));
            customer = (CustomerModel) dao.getById(Integer.parseInt(req.getParameter("id")));
            System.out.println(customer.toString());
            try{
                String name = req.getParameter("name");
                if(name != null){
                    customer.setName(name);

                }
            }catch(NullPointerException e){}
            try{
                BigInteger phoneNumber = new BigInteger(req.getParameter("phoneNumber"));
                if(phoneNumber.intValue() > 0) {
                    customer.setPhoneNumber(phoneNumber);

                }
            }catch(NullPointerException e){} catch (NumberFormatException e){}
            try{
                DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                Date myDate = null;
                java.sql.Date sqlDate = null;
                try {
                    myDate = (Date) formatter.parse(req.getParameter("joiningDate"));
                    sqlDate = new java.sql.Date(myDate.getTime());
                    customer.setJoiningDate(sqlDate);
                } catch (ParseException e) {}
            }catch(NullPointerException e){}
            try{
                String address = req.getParameter("address");
                if(address != null){
                    customer.setAddress(address);
                }
            }catch(NullPointerException e){}
            System.out.println(customer.toString());
            out.print(dao.update(customer));
        }catch(NullPointerException e){e.printStackTrace();}
    }
}
