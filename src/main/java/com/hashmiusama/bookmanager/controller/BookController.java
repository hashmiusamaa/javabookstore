package com.hashmiusama.bookmanager.controller;

import com.hashmiusama.bookmanager.daoimpl.BookDAO;
import com.hashmiusama.bookmanager.models.BookModel;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by uhashmi on 5/26/2016.
 */
@WebServlet(name = "BookController",
        urlPatterns = "/book")
public class BookController extends HttpServlet {
    BookDAO dao = new BookDAO();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = "nulls";
        String name = "nulls";
        String author = "nulls";
        BookModel book = new BookModel();
        ArrayList<Object> books = null;
        PrintWriter out = resp.getWriter();
        try{
            name = req.getParameter("name");
            if(!name.equals("nulls")){
                try{
                    books = dao.getByName(name);
                    System.out.println(books.toString());
                    out.print(books.toString());
                }catch(NullPointerException e){
                    System.out.println("[]");
                    e.printStackTrace();
                }
            }
        }
        catch(NullPointerException e){}
        try{
            String idea = req.getParameter("id");
            id = idea;
            if(!id.equals("nulls")){
                try {
                    book = (BookModel) dao.getById(Integer.parseInt(id));
                    out.print(book.toString());
                }catch(NullPointerException e){
                    e.printStackTrace();
                    out.println("{}");
                }
            }
        }
        catch(NullPointerException e){}
        try{
            author = req.getParameter("author");
            if(!author.equals("nulls")){
                try{
                    books = dao.getByAuthor(author);
                    System.out.println(books.toString());
                    out.print(books.toString());
                }catch(NullPointerException e){
                    System.out.println("[]");
                    e.printStackTrace();
                }
            }
        }
        catch(NullPointerException e){}
        if(req.getParameter("author") == null && req.getParameter("name") == null && req.getParameter("id") == null){
            books = dao.getAll();
            out.print(books.toString());
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try{
            int id = Integer.parseInt(req.getParameter("id"));
            BookModel book = (BookModel) dao.getById(id);
//            System.out.println();
            resp.getWriter().print(dao.delete(book));
        }catch(NullPointerException e){}
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        BookModel bookModel = new BookModel();
        PrintWriter out = resp.getWriter();
        try{
            bookModel.setName(req.getParameter("name"));
            bookModel.setAuthor(req.getParameter("author"));
            bookModel.setPublisher(req.getParameter("publisher"));
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date myDate = null;
            java.sql.Date sqlDate = null;
            try {
                myDate = (Date) formatter.parse(req.getParameter("date"));
                sqlDate = new java.sql.Date(myDate.getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            bookModel.setDate(sqlDate);
            bookModel.setPrice(Integer.parseInt(req.getParameter("price")));
            System.out.println("inserting "+bookModel.toString());
            if(dao.insert(bookModel)){
                out.print(true);
            }else{
                out.print(false);
            }
        }catch(NullPointerException e){e.printStackTrace();}
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        BookModel book = new BookModel();
        PrintWriter out = resp.getWriter();
        try{
            int id = -1;
            try {
               id = Integer.parseInt(req.getParameter("id"));
               book = (BookModel) dao.getById(id);
            }catch(Exception e){}
            System.out.println(book.toString());
            try{
                String name = req.getParameter("name");
                System.out.println("Setting book name");
                if(name != null){
                    book.setName(name);
                    System.out.println("book name set to "+name);
                }
            }catch(NullPointerException e){}
            try{
                int price = Integer.parseInt(req.getParameter("price"));
                System.out.println("Setting book price");
                if(price > 0) {
                    book.setPrice(price);
                    System.out.println("Price set to " + price);
                }
            }catch(NullPointerException e){} catch (NumberFormatException e){}
            try{
                DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                Date myDate = null;
                java.sql.Date sqlDate = null;
                try {
                    myDate = (Date) formatter.parse(req.getParameter("date"));
                    System.out.println("date "+myDate);
                    sqlDate = new java.sql.Date(myDate.getTime());
                    System.out.println("sqldate " + sqlDate);
                    System.out.println("Setting book date");
                    book.setDate(sqlDate);
                    System.out.println("Date set to "+sqlDate);
                } catch (ParseException e) {}
            }catch(NullPointerException e){}
            try{
                String publisher = req.getParameter("publisher");
                System.out.println("Setting book publisher");
                if(publisher != null){
                    book.setPublisher(publisher);
                }
            }catch(NullPointerException e){}
            try{
                String author = req.getParameter("author");
                System.out.println("Setting book author");
                if(author != null) {
                    book.setAuthor(author);
                    System.out.println("author set to " + author);
                }
            }catch(NullPointerException e){}
            BookDAO newDao = new BookDAO();
            System.out.println(book.toString());
            out.print(newDao.update(book));
        }catch(NullPointerException e){e.printStackTrace();}
    }
}