package com.hashmiusama.bookmanager.controller;

import com.hashmiusama.bookmanager.daoimpl.SupervisorDAO;
import com.hashmiusama.bookmanager.models.SupervisorModel;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by uhashmi on 5/31/2016.
 */
@WebServlet("/login")
public class LoginController extends HttpServlet{
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        SupervisorDAO dao = new SupervisorDAO();
        PrintWriter out = resp.getWriter();
        String password = "";
        String userName = "";
        SupervisorModel supervisor = new SupervisorModel();
        supervisor.setPassword("0");
        try {
             userName = req.getParameter("username");
            try {
                supervisor = (SupervisorModel) dao.getByUsername(userName);
                try {
                    password = req.getParameter("password");
                    if(password.equals(supervisor.getPassword())){
                        out.print(supervisor.toString());
                    }else{
                        out.print("err password");
                    }
                }catch(NullPointerException e){
                    out.print("err");
                }
            }catch(Exception E){
                out.print("err username");
            }
        }catch(NullPointerException e){
            out.print("err");
        }
    }
}
