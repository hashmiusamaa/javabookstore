package com.hashmiusama.bookmanager.controller;

import com.hashmiusama.bookmanager.daoimpl.SupervisorDAO;
import com.hashmiusama.bookmanager.models.SupervisorModel;
import org.hibernate.exception.ConstraintViolationException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.ArrayList;


/**
 * Created by uhashmi on 5/30/2016.
 */
@WebServlet(name = "SupervisorController",
        urlPatterns = "/supervisor")
public class SupervisorController extends HttpServlet{
    SupervisorDAO dao = new SupervisorDAO();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = "nulls";
        String name = "nulls";
        String phoneNumber = "nulls";
        SupervisorModel supervisor= new SupervisorModel();
        ArrayList<Object> supervisors = null;
        PrintWriter out = resp.getWriter();
        try{
            name = req.getParameter("username");
            if(!name.equals("nulls")){
                try{
                    supervisor = (SupervisorModel)dao.getByUsername(name);
                    System.out.println(supervisors.toString());
                    out.print(supervisors.toString());
                }catch(NullPointerException e){
                    System.out.println("{}");
                    e.printStackTrace();
                }
            }
        }
        catch(NullPointerException e){}
        try{
            String idea = req.getParameter("id");
            id = idea;
            if(!id.equals("nulls")){
                try {
                    supervisor = (SupervisorModel) dao.getById(Integer.parseInt(id));
                    out.print(supervisor.toString());
                }catch(NullPointerException e){
                    e.printStackTrace();
                    out.println("{}");
                }
            }
        }
        catch(NullPointerException e){}
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try{
            int id = Integer.parseInt(req.getParameter("id"));
            SupervisorModel supervisor = (SupervisorModel) dao.getById(id);
            resp.getWriter().print(dao.delete(supervisor));
        }catch(NullPointerException e){}
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        SupervisorModel supervisor = new SupervisorModel();
        PrintWriter out = resp.getWriter();
        try{
            supervisor.setUsername(req.getParameter("username"));
            supervisor.setPassword(req.getParameter("password"));
            supervisor.setPhoneNumber(new BigInteger(req.getParameter("phoneNumber")));
            supervisor.setEmail(req.getParameter("email"));
            System.out.println("inserting "+supervisor.toString());
                out.print(dao.insert(supervisor));
        }catch(NullPointerException e){e.printStackTrace();
        }catch(ConstraintViolationException e){e.printStackTrace();}
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        SupervisorModel supervisor = new SupervisorModel();
        PrintWriter out = resp.getWriter();
        try{
            System.out.println(Integer.parseInt(req.getParameter("id")));
            supervisor = (SupervisorModel) dao.getById(Integer.parseInt(req.getParameter("id")));
            System.out.println(supervisor.toString());
            try{
                String name = req.getParameter("username");
                if(name != null){
                    supervisor.setUsername(name);

                }
            }catch(NullPointerException e){}
            try{
                BigInteger phoneNumber = new BigInteger(req.getParameter("phoneNumber"));
                if(phoneNumber.intValue() > 0) {
                    supervisor.setPhoneNumber(phoneNumber);

                }
            }catch(NullPointerException e){} catch (NumberFormatException e){}
            try{
                String email = req.getParameter("email");
                if(email != null){
                    supervisor.setEmail(email);
                }
            }catch(NullPointerException e){}
            try{
                String password = req.getParameter("password");
                if(password != null){
                    supervisor.setEmail(password);
                }
            }catch(NullPointerException e){}
            System.out.println(supervisor.toString());
            out.print(dao.update(supervisor));
        }catch(NullPointerException e){e.printStackTrace();}
    }
}
