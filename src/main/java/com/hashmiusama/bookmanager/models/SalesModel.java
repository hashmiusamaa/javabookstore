package com.hashmiusama.bookmanager.models;

import javax.persistence.*;

/**
 * Created by uhashmi on 5/31/2016.
 */
@Entity
@Table(name="sales")
public class SalesModel {
    @Id
    @GeneratedValue
    int id;
    @Column(name="supervisorid")
    int supervisorId;
    @Column(name="bookid")
    int bookId;
    @Column(name="customerid")
    int customerId;

    public SalesModel(){}

    public SalesModel(int supervisorid, int bookid, int customerid) {
        this.supervisorId = supervisorid;
        this.bookId = bookid;
        this.customerId = customerid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSupervisorId() {
        return supervisorId;
    }

    public void setSupervisorId(int supervisorid) {
        this.supervisorId = supervisorid;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookid) {
        this.bookId = bookid;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerid) {
        this.customerId = customerid;
    }

    @Override
    public String toString() {
        return "{" +
                "\"id\":\"" + id + "\""+
                ", \"supervisorid\":\"" + supervisorId +"\""+
                ", \"bookid\":\"" + bookId +"\""+
                ", \"customerid\":\"" + customerId +"\""+
                '}';
    }
}
