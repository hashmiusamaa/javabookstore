package com.hashmiusama.bookmanager.models;

import javax.persistence.*;
import java.sql.Date;

/**
 * Created by uhashmi on 5/26/2016.
 */
@Entity
@Table(name="books")
public class BookModel {
    @Id
    @GeneratedValue()
    private int id;
    private String name;
    private String author;
    private String publisher;
    private Date date;
    private float price;

    public BookModel(){
    }

    public BookModel(String name, String author, String publisher, Date date, float price) {
        this.name = name;
        this.author = author;
        this.publisher = publisher;
        this.date = date;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "{" +
                "\"id\":\"" + id + '\"' +
                ", \"name\":\"" + name + '\"' +
                ", \"author\":\"" + author + '\"' +
                ", \"publisher\":\"" + publisher + '\"' +
                ", \"date\":\"" + date +'\"' +
                ", \"price\":\"" + price + '\"'+
                '}';
    }
}
