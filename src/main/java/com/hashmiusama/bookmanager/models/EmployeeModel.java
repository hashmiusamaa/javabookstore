package com.hashmiusama.bookmanager.models;

import javax.persistence.*;
import java.math.BigInteger;
import java.sql.Date;

/**
 * Created by uhashmi on 5/26/2016.
 */
@Entity
@Table(name="employees")
public class EmployeeModel {
    @Id
    @GeneratedValue()
    private int id;
    private String name;
    private String address;
    @Column(name="joiningdate")
    private Date joiningDate;
    @Column(name="phonenumber")
    private BigInteger phoneNumber;

    public EmployeeModel(){}

    public EmployeeModel(String name, String address, Date dateOfJoining, BigInteger phoneNumber) {
        this.name = name;
        this.address = address;
        this.joiningDate = dateOfJoining;
        this.phoneNumber = phoneNumber;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getJoiningDate() {
        return joiningDate;
    }

    public void setJoiningDate(Date joiningDate) {
        this.joiningDate = joiningDate;
    }

    public BigInteger getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(BigInteger phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString() {
        return "{" +
                "\"joiningDate\":\"" + joiningDate +
                "\", \"address\":\"" + address + '\"' +
                ", \"name\":\"" + name + '\"' +
                ", \"id\":\"" + id + "\"" +
                ", \"phoneNumber\":\""+phoneNumber +"\""+
                "}";
    }
}
