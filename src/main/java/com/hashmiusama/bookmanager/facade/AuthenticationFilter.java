package com.hashmiusama.bookmanager.facade;

import com.hashmiusama.bookmanager.controller.BookController;
import com.hashmiusama.bookmanager.daoimpl.SupervisorDAO;
import com.hashmiusama.bookmanager.models.SupervisorModel;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.text.ParseException;

/**
 * Created by uhashmi on 5/31/2016.
 */
@WebFilter(servletNames = {"BookController","CustomerController","EmployeeController","SalesController"})
public class AuthenticationFilter implements Filter{
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        int id = -1;
        System.out.println("Filter Has Been Hit.");
        HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;
        try {
            id = Integer.parseInt(httpRequest.getHeader("sid"));
        }catch (NullPointerException e){

        }catch(Exception e){

        }finally {
            if(id == -1){servletResponse.getWriter().print("unauthorised");}else{
                try{
                    SupervisorDAO dao = new SupervisorDAO();
                    SupervisorModel supervisor = (SupervisorModel) dao.getById(id);
                    if(supervisor.getId() == id){
                        filterChain.doFilter(servletRequest,servletResponse);
                    }else{
                        servletResponse.getWriter().print("unauthorised");
                    }
                }catch(NullPointerException e){
                    servletResponse.getWriter().print("unauthorised");
                }
            }
        }
    }

    public void destroy() {

    }
}
